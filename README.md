<p align="center">
  <img src="https://www.devninjas.com.br/wp-content/themes/understrap-child/img/logo-devninjas.png" width="250">
</p>

# < front >Developer</ end >

Objetivo deste desafio é avaliarmos o seu domínio em front-end, ou seja, sua organização, estilo e boas práticas com o código, conhecimento dos frameworks e tecnologias utilizadas.

## Regras

1. Todo o seu código deve ser disponibilizado num repositório **público** ou **privado** em seu Github, Gitlab ou Bitbucket 
pessoal. Envie o link para contato@devninjas.com.br ou faça um pull-request;

2. Desenvolver o projeto utilizando:  

- HTML e CSS (ou algum pré-processador);
- Algum framework CSS (Atualmente estamos trabalhando com Bootstrap, mas use o que achar melhor);
- Layout responsivo;

## O Desafio

Este é o layout que deverá ser desenvolvido:
![layout one page](layout-onepage.png)

E aqui os ícones e imagens do layout:
[Download do arquivo](mockup)


## Dúvidas

Envie suas dúvidas diretamente para contato@devninjas.com.br ou abrindo uma issue.
